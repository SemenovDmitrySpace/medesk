import React, { createRef, useEffect, useRef } from 'react'

import './styles.css'

interface IProps {
  opened: boolean
}

const Collapse: React.FC<IProps> = ({ opened, children }) => {

  const containerRef = createRef<HTMLDivElement>()
  const contentRef = createRef<HTMLDivElement>()

  const height = useRef<number>(0)

  useEffect(() => {
    const nextHeight = contentRef.current?.offsetHeight || 0
    height.current = nextHeight 
  }, [opened, contentRef])

  return (
    <div
      className={'collapse__container'}
      style={
        opened 
          ? { height: height.current }
          : { height: '0px' }
      }
      ref={containerRef}
    >
      <div 
        className="collapse__content"
        ref={contentRef}  
      >
        { children }
      </div>
    </div>
  )
}

export default Collapse
