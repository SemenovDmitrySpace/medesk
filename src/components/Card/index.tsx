import React, { ReactNode, useCallback, useState } from "react";
import { MdKeyboardArrowDown } from 'react-icons/md'

import Collapse from '../Collapse'

import "./styles.css";

interface IProps {
  summary: string
  content: string
  icon?: ReactNode
}

const Cart: React.FC<IProps> = ({ summary, content, icon }) => {

  const [ opened, setOpened ] = useState(false)

  const toggle = useCallback(() => setOpened((prevState) => !prevState), [])

  return (
    <div className="cart__container">
      <div
        className="cart__summary"
        tabIndex={0}
        role="button"
        onClick={toggle}
      >
        <div className="cart__summary-content">
          <p className="cart__summary-label">{ summary }</p>
        </div>
        <MdKeyboardArrowDown 
          className={`cart__summary-icon ${opened ? 'cart__summary-icon--open' : ''}`}
          size={'24px'} 
        />
      </div>
      <Collapse opened={opened}>
        <div className="cart__content">
          { icon && (
            <div className="cart__content-icon-wrapper">
              { icon }
            </div>
          )}
          <p>
            { content }
          </p>
        </div>
      </Collapse>
    </div>
  );
};

export default Cart;
