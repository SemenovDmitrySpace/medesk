import React from 'react';
import { MdOutlineLanguage } from 'react-icons/md'

import Container from './components/Container'
import Card from './components/Card'

const App: React.FC = () => {
  return (
    <Container>
      <Card 
        summary='Information'
        content='Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget.'
        icon={<MdOutlineLanguage size={'24px'} />}
      />
      <Card 
        summary='Information long - Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget.'
        content='Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
        eget.'
      />
    </Container>
  );
}

export default App;
